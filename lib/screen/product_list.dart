import 'dart:convert';
import 'dart:developer';

import 'package:assignment/NetworkClass/network_class.dart';
import 'package:assignment/NetworkClass/network_response.dart';
import 'package:assignment/screen/product_detail.dart';
import 'package:assignment/utils/common_method.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../NetworkClass/web_url.dart';
import '../model/product_model.dart';
import '../utils/custom_loader.dart';

class ProductListScreen extends StatefulWidget {
  const ProductListScreen({Key? key}) : super(key: key);

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen>
    implements NetworkResponse {
  Widget appBarTitle = Text(
    "Home ",
    style: TextStyle(color: Colors.white),
  );
  Icon actionIcon = Icon(
    Icons.search,
    color: Colors.white,
  );
  List<ProductModel> productList = [];
  List<ProductModel> searchList = [];
  ScrollController? _controller;
  bool isList = false;
  bool isSearch = false;
  bool isSearchList = false;
  String noData = '';

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      init();
    });

    super.initState();
  }

  void init() async {
    callGetProductApi();
  }

  double screenHeight = 0;
  double screenWidth = 0;
  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.orange,
        title: isSearch ? searchWidget() : appBarTitle,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                isSearch = !isSearch;

                if (isSearch == false) {
                  searchList.clear();
                  isSearchList = false;
                  noData = '';
                  isList = true;
                }
              });
            },
            icon: isSearch ? Icon(Icons.clear) : Icon(Icons.search),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
        child: productListWidget(),
      ),
    );
  }

  Widget searchWidget() {
    return TextFormField(
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
        hintText: 'Search here...',
        hintStyle: TextStyle(color: Colors.white),
        prefixIcon: Icon(
          Icons.search,
          color: Colors.white,
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      onChanged: (value) {
        setState(() {
          if (value.length > 0) {
            log('Value====>  $value');
            searchList.clear();
            productList.forEach((element) {
              if (element.mrp.startsWith(capitalize(value)) ||
                  element.mrp.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.name.startsWith(capitalize(value)) ||
                  element.name.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.option.startsWith(capitalize(value)) ||
                  element.option.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.qr.startsWith(capitalize(value)) ||
                  element.qr.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.fit.startsWith(capitalize(value)) ||
                  element.fit.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.finish.startsWith(capitalize(value)) ||
                  element.finish.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.category.startsWith(capitalize(value)) ||
                  element.category.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.collection.startsWith(capitalize(value)) ||
                  element.collection.toLowerCase().startsWith(value)) {
                searchList.add(element);
              } else if (element.subCategory.startsWith(capitalize(value)) ||
                  element.subCategory.toLowerCase().startsWith(value)) {
                searchList.add(element);
              }
            });
            if (searchList.length > 0) {
              isSearchList = true;
              isList = true;
            } else {
              isSearchList = false;
              isList = false;
              noData = 'No product found!';
              searchList.clear();
            }
            log('ListData==>  ${searchList.toString()}');
            isSearch = true;
          } else {
            isSearch = false;
            searchList.clear();
            isSearchList = false;
            value = '';
            noData = '';
            isList = true;
          }
        });
      },
    );
  }

/*  */
  Widget productListWidget() {
    return Container(
      child: isList
          ? isSearchList
              ? ListView.builder(
                  controller: _controller,
                  itemCount: searchList.length,
                  itemBuilder: (context, index) => productItem(index),
                )
              : ListView.builder(
                  controller: _controller,
                  itemCount: productList.length,
                  itemBuilder: (context, index) => productItem(index),
                )
          : Center(
              child: Text(noData),
            ),
    );
  }

  Widget productItem(int index) {
    var item = isSearchList ? searchList[index] : productList[index];
    String image =
        'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/1665px-No-Image-Placeholder.svg.png';

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProductDetailScreen(
              model: item,
            ),
          ),
        );
      },
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Container(
          height: 130,
          width: screenWidth,
          margin: EdgeInsets.symmetric(vertical: 5),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 5,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(item.name),
                    Icon(
                      (Icons.keyboard_arrow_up),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 80,
                    width: 80,
                    margin: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(item.imageUrl),
                        fit: BoxFit.contain,
                      ),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: item.imageUrl,
                      imageBuilder: (context, imgProvider) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                              image: imgProvider, fit: BoxFit.contain),
                        ),
                      ),
                      placeholder: (context, url) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                              image: NetworkImage(image), fit: BoxFit.fill),
                        ),
                      ),
                      errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          image: DecorationImage(
                              image: NetworkImage(image), fit: BoxFit.fill),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          item.finish,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          'OP: ${item.option}',
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                        Text(
                          'MAX: ${item.mrp}',
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                        Container(
                            height: 34,
                            width: screenWidth * 0.58,
                            child: Row(
                              children: [
                                Text('Sizes: '),
                                Expanded(
                                    child: pSizeWidget(item.availableSize)),
                              ],
                            )),
                      ],
                    ),
                  ),
                  /*  Container(
                    width: 80,
                    margin: EdgeInsets.only(right: 8),
                    child: Column(
                      children: [
                        Container(child: pSizeWidget(item.availableSize)),
                      ],
                    ),
                  ) */
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget pSizeWidget(List<String> sizeList) {
    return ListView.builder(
        // shrinkWrap: true,
        // physics: NeverScrollableScrollPhysics(),
        itemCount: sizeList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          var item = sizeList[index];
          return Card(
              child: Padding(
            padding: EdgeInsets.all(5.0),
            child: Text(item),
          ));
        });
  }

  void callGetProductApi() async {
    NetworkClass(GET_PRODUCT, this, REQ_GET_PRODUCT)
        .callGetService(context, true);
  }

  @override
  void onError({Key? key, int? requestCode, String? response}) {
    // TODO: implement onError
    switch (requestCode) {
      case REQ_GET_PRODUCT:
        log('Error======>  ');
        break;
    }
  }

  @override
  void onResponse({Key? key, int? requestCode, String? response}) {
    // TODO: implement onResponse
    try {
      switch (requestCode) {
        case REQ_GET_PRODUCT:
          log('message======>  $response');
          var map = jsonDecode(response!);
          var _productList = map['Products'] as List;

          log('Data========  ${map['Products']}');
          setState(() {
            productList.clear();
            var listData =
                _productList.map((e) => ProductModel.fromJson(e)).toList();
            productList.addAll(listData);

            if (productList.isNotEmpty) {
              isList = true;
              noData = '';
            } else {
              isList = false;
              noData = 'No data available';
            }
          });
          break;
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
