import 'package:assignment/model/product_model.dart';
import 'package:flutter/material.dart';

class ProductDetailScreen extends StatefulWidget {
  ProductDetailScreen({Key? key, required this.model}) : super(key: key);
  ProductModel model;

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  late ProductModel detail;
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      init();
    });
    super.initState();
  }

  void init() async {
    detail = widget.model;
  }

  double screenHeight = 0;
  double screenWidth = 0;

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.orange,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text(
          widget.model.name,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 15),
        padding: EdgeInsets.only(bottom: 8),
        child: ListView(
          children: [
            imageContainer(),
            SizedBox(
              height: 12,
            ),
            priceSizeContainer(),
            titleContainer('Brand', widget.model.brand),
            titleContainer('Category', widget.model.category),
            titleContainer('SubCategory', widget.model.subCategory),
            titleContainer('Collection', widget.model.collection),
            titleContainer('Gender', widget.model.gender),
            titleContainer('Name', widget.model.name),
            titleContainer('Fit', widget.model.fit),
            titleContainer('Theme', widget.model.theme),
            titleContainer('Finish', widget.model.finish),
            titleContainer(
              'Offer Months',
              widget.model.offerMonths
                  .toString()
                  .replaceAll('[', '')
                  .replaceAll(']', ''),
            ),
            titleContainer('Description', widget.model.description),
            titleContainer('Oprtion', widget.model.option),
            titleContainer('QRCode', widget.model.qr),
            titleContainer('Material', widget.model.material),
            titleContainer('Quality', widget.model.quality),
          ],
        ),
      ),
    );
  }

  Widget imageContainer() {
    return Container(
      height: screenHeight * 0.3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        image: DecorationImage(
            image: NetworkImage(widget.model.imageUrl), fit: BoxFit.contain),
      ),
    );
  }

  Widget priceSizeContainer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8),
          child: Text(
            'MRP: ${widget.model.mrp}',
            style: TextStyle(
              color: Colors.grey.shade600,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        ),
        Container(
          height: 30,
          margin: EdgeInsets.only(right: 8),
          width: screenWidth * 0.52,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.model.availableSize.length,
            itemBuilder: (context, index) => Card(
              child: Container(
                height: 25,
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                alignment: Alignment.center,
                child: Text(
                  widget.model.availableSize[index],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget titleContainer(String title, String subTitle) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                subTitle,
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.grey.shade600,
                ),
              ),
            ],
          )),
    );
  }
}
