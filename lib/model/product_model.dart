import 'dart:convert';

class ProductModel {
  String id = '';
  String brand = '';
  String mood = '';
  String gender = '';
  String theme = '';
  String category = '';
  String name = '';
  String color = '';
  String option = '';
  String mrp = '';
  String subCategory = '';
  String collection = '';
  String fit = '';
  String description = '';
  String qr = '';
  String fabrics = '';
  String finish = '';
  String productClass = '';
  String material = '';
  String quality = '';
  String displayName = '';
  String displayOrder = '';
  String minimumQuantity = '';
  String maximumQuantity = '';
  String image = '';
  String imaegAlt = '';
  String imageUrl = '';
  String technology = '';
  String technologyImage = '';
  // String imageUrl = '';
  List<String> availableSize = [];
  List<String> offerMonths = [];

  ProductModel({
    required this.brand,
    required this.mood,
    required this.gender,
    required this.name,
    required this.color,
    required this.option,
    required this.mrp,
    required this.collection,
    required this.fit,
    required this.description,
    required this.qr,
    required this.fabrics,
    required this.finish,
    required this.productClass,
    required this.material,
    required this.quality,
    required this.displayName,
    required this.displayOrder,
    required this.minimumQuantity,
    required this.maximumQuantity,
    required this.theme,
    required this.category,
    required this.subCategory,
    required this.image,
    required this.imageUrl,
    required this.imaegAlt,
    required this.technology,
    required this.technologyImage,
    required this.availableSize,
    required this.offerMonths,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    var size = json['AvailableSizes'] ?? [];
    var offer = json['OfferMonths'] ?? [];
    return ProductModel(
      brand: json['Brand'].toString(),
      mood: json['Mood'].toString(),
      gender: json['Gender'].toString(),
      name: json['Name'].toString(),
      color: json['Color'].toString(),
      option: json['Option'].toString(),
      mrp: json['MRP'].toString(),
      collection: json['Collection'].toString(),
      fit: json['Fit'].toString(),
      description: json['Description'].toString(),
      qr: json['QRCode'].toString(),
      fabrics: json['Fabrics'].toString(),
      finish: json['Finish'].toString(),
      productClass: json['ProductClass'].toString(),
      material: json['Material'].toString(),
      quality: json['Quality'].toString(),
      displayName: json['DisplayName'].toString(),
      displayOrder: json['DisplayOrder'].toString(),
      minimumQuantity: json['MinQuantity'].toString(),
      maximumQuantity: json['MaxQuantity'].toString(),
      theme: json['Theme'].toString(),
      category: json['Category'].toString(),
      subCategory: json['SubCategory'].toString(),
      image: json['Image'].toString(),
      imageUrl: json['ImageUrl'].toString(),
      imaegAlt: json['ImageAlt'].toString(),
      technology: json['Technology'].toString(),
      technologyImage: json['TechnologyImageUrl'].toString(),
      availableSize: List<String>.from(size),
      offerMonths: List<String>.from(offer),
    );
  }
}
